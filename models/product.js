import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { composeWithMongoose } from "graphql-compose-mongoose"

const { Schema } = mongoose

export const ProductSchema = new Schema(
  {
    name: {
      type: String,
    },
    category: String,
    shopsMeta: [
      {
        shop: { type: Schema.Types.ObjectId, ref: "Shop" },
        price: Number,
      },
    ],
  },
  {
    collection: "products",
  }
)

ProductSchema.plugin(timestamps)

ProductSchema.index({ createdAt: 1, updatedAt: 1 })

export const Product = mongoose.model("Product", ProductSchema)
export const ProductTC = composeWithMongoose(Product)
