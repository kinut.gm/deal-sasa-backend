import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { composeWithMongoose } from "graphql-compose-mongoose"

const { Schema } = mongoose

export const CartSchema = new Schema(
  {
    productId: {
      type: Schema.Types.ObjectId,
      ref: "Product",
    },
    shopId: {
      type: Schema.Types.ObjectId,
      ref: "Shop",
    },
    customerId: {
      type: Schema.Types.ObjectId,
      ref: "Customer",
    },
    quantity: { type: Number },
    insertionDate: { type: Date, default: Date.now },
    orderDate: { type: Date },
    deliveryDate: { type: Date },
    cancelled: { type: Boolean },
  },
  {
    collection: "cart",
  }
)

CartSchema.plugin(timestamps)

CartSchema.index({ createdAt: 1, updatedAt: 1 })

export const Cart = mongoose.model("Cart", CartSchema)
export const CartTC = composeWithMongoose(Cart)
