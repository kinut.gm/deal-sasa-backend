import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { composeWithMongoose } from "graphql-compose-mongoose"

const { Schema } = mongoose

export const CustomerSchema = new Schema(
  {
    name: {
      type: String,
    },
    email: {
      type: String,
    },
    password: { type: String },
    phoneNumber: { type: String },
  },
  {
    collection: "customers",
  }
)

CustomerSchema.plugin(timestamps)

CustomerSchema.index({ createdAt: 1, updatedAt: 1 })

export const Customer = mongoose.model("Customer", CustomerSchema)
export const CustomerTC = composeWithMongoose(Customer)
