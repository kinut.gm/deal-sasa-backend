import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { composeWithMongoose } from "graphql-compose-mongoose"

const { Schema } = mongoose

export const TodoSchema = new Schema(
  {
    name: {
      type: String,
    },
    done: { type: Boolean, default: false },
  },
  {
    collection: "todos",
  }
)

TodoSchema.plugin(timestamps)

TodoSchema.index({ createdAt: 1, updatedAt: 1 })

export const Todo = mongoose.model("Todo", TodoSchema)
export const TodoTC = composeWithMongoose(Todo)
