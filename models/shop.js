import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { composeWithMongoose } from "graphql-compose-mongoose"

const { Schema } = mongoose

export const ShopSchema = new Schema(
  {
    name: {
      type: String,
    },
    email: {
      type: String,
    },
    password: { type: String },
    phoneNumber: { type: String },
  },
  {
    collection: "shops",
  }
)

ShopSchema.plugin(timestamps)

ShopSchema.index({ createdAt: 1, updatedAt: 1 })

export const Shop = mongoose.model("Shop", ShopSchema)
export const ShopTC = composeWithMongoose(Shop)
