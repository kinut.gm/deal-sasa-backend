import dotenv from "dotenv"
import express from "express"
import { ApolloServer } from "apollo-server-express"
import { createServer } from "http"
import { ApolloServerPluginDrainHttpServer } from "apollo-server-core"
import { makeExecutableSchema } from "@graphql-tools/schema"
import { WebSocketServer } from "ws"
import { useServer } from "graphql-ws/lib/use/ws"
import path from "path"

import mongoose from "mongoose"

import schema from "./schemas/main.js"

dotenv.config()

const app = express()

const httpServer = createServer(app)

app.get("/graphql", (req, res) => {
  res.sendFile(path.join(path.resolve(), "./IDE.html"))
})

// Creating the WebSocket server
const wsServer = new WebSocketServer({
  // This is the `httpServer` we created in a previous step.
  server: httpServer,
  // Pass a different path here if your ApolloServer serves at
  // a different path.
  path: "/graphql",
})

// Hand in the schema we just created and have the
// WebSocketServer start listening.
const serverCleanup = useServer({ schema }, wsServer)

mongoose.Promise = global.Promise

const connection = mongoose.connect(process.env.MONGODB_URI, {
  // autoIndex: true,
  // reconnectTries: Number.MAX_VALUE,
  // reconnectInterval: 500,
  // poolSize: 50,
  // bufferMaxEntries: 0,
  // keepAlive: 120,
  useNewUrlParser: true,
})

// mongoose.set("useCreateIndex", true)

connection
  .then((db) => console.log("DB connected"))
  .catch((err) => {
    console.log(err)
  })

const server = new ApolloServer({
  schema,
  cors: true,
  playground: process.env.NODE_ENV === "development" ? true : false,
  introspection: true,
  tracing: true,
  path: "/",
  plugins: [
    // Proper shutdown for the HTTP server.
    ApolloServerPluginDrainHttpServer({ httpServer }),

    // Proper shutdown for the WebSocket server.
    {
      async serverWillStart() {
        return {
          async drainServer() {
            await serverCleanup.dispose()
          },
        }
      },
    },
  ],
})

await server.start()

server.applyMiddleware({
  app,
  path: "/",
  cors: true,
  onHealthCheck: () =>
    // eslint-disable-next-line no-undef
    new Promise((resolve, reject) => {
      if (connection.readyState > 0) {
        console.log("readyState > 0")
        resolve()
      } else {
        reject()
      }
    }),
})

// httpServer.listen({ port: process.env.PORT }, () => {
//   console.log(`🚀 Server listening on port ${process.env.PORT}`)
//   console.log(`😷 Health checks available at ${process.env.HEALTH_ENDPOINT}`)
// })

httpServer.listen(process.env.PORT, () => {
  useServer({ schema }, wsServer)
  console.log(`🚀 Server runnning on port ${process.env.PORT}`)
})
