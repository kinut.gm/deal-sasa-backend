import { SchemaComposer } from "graphql-compose"
import { PubSub } from "graphql-subscriptions"

import { CustomerQuery, CustomerMutation } from "./customer.js"
import { CartQuery, CartMutation } from "./cart.js"
import { ProductQuery, ProductMutation } from "./product.js"
import { ShopQuery, ShopMutation } from "./shop.js"
import { Product, ProductTC } from "../models/product.js"
import { TodoQuery, TodoMutation } from "./todo.js"
import { Cart, CartTC } from "../models/cart.js"
import { TodoTC } from "../models/todo.js"
import { Shop } from "../models/shop.js"

const pubsub = new PubSub()

const schemaComposer = new SchemaComposer()

schemaComposer.Query.addFields({
  ...CustomerQuery,
  ...CartQuery,
  ...ProductQuery,
  ...ShopQuery,
  ...TodoQuery,
})

schemaComposer.Mutation.addFields({
  ...CustomerMutation,
  ...CartMutation,
  ...ProductMutation,
  ...ShopMutation,
  ...TodoMutation,
})

const mCartTC = schemaComposer.createObjectTC(
  `
  type mCart {
    shop: mShop
    product: mProduct
    quantity: Float
    deliveryDate : Date
    cancelled : Boolean
    orderDate: Date
    id:ID
    insertionDate: Date
    price: Float
  }

  type mShop {
    name: String
    id: ID    
  }

  type mProduct {
    name: String
    id: ID
  }
`
)

const mProductTC = schemaComposer.createObjectTC(
  `
  type mProduct {
    name: String
    category: String
    shops : [mShop2]
    id: ID
  } 

  type mShop2 {
    name: String
    price: Float
    id: ID
  }  
  `
)

const _mShopTC = schemaComposer.createObjectTC(`
  type _mShop {
    id: ID
    name: String
    email: String
    password : String
    phoneNumber: String
    products: [_mProduct]
  } 

  type _mProduct {
    name: String
    price: Float
    id: ID
  }  
  `)

schemaComposer.Query.addFields({
  cartPopulate: {
    type: [mCartTC],
    args: { customer: "ID" },
    resolve: async (source, args, context, info) => {
      const cartItems = await Cart.find({ customerId: args.customer })
        .populate("productId")
        .populate("shopId")

      const getPrice = (shopId, shopsArray) => {
        let price = shopsArray.filter(
          (shopMeta) => shopMeta.shop.valueOf() == shopId
        )[0].price

        return price
      }

      let mCart = []

      cartItems.forEach((cartItem) => {
        let mCartItem = {
          shop: {
            name: cartItem.shopId.name,
            id: cartItem.shopId._id,
          },
          product: {
            name: cartItem.productId.name,
            id: cartItem.productId._id,
          },
          quantity: cartItem.quantity,
          deliveryDate: cartItem.deliveryDate,
          cancelled: cartItem.cancelled,
          orderDate: cartItem.orderDate,
          id: cartItem._id,
          insertionDate: cartItem.insertionDate,
          price: getPrice(
            cartItem.shopId._id.valueOf(),
            cartItem.productId.shopsMeta
          ),
        }

        mCart[mCart.length] = mCartItem
      })

      console.log(mCart)

      return mCart
    },
  },

  productsPopulate: {
    type: [mProductTC],
    args: null,
    resolve: async (source, args, context, info) => {
      const products = await Product.find()
        .sort({ createdAt: -1 })
        .populate("shopsMeta.shop")

      const getShops = (shopsMeta) => {
        let mShops = []

        shopsMeta.forEach((shopMeta) => {
          let mShop = {
            name: shopMeta.shop.name,
            price: shopMeta.price,
            id: shopMeta.shop._id,
          }

          mShops[mShops.length] = mShop
        })

        return mShops
      }

      let mProducts = []

      products.forEach((product) => {
        let mProduct = {
          name: product.name,
          category: product.category,
          id: product._id,
          shops: getShops(product.shopsMeta),
        }

        mProducts[mProducts.length] = mProduct
      })

      console.log(mProducts)

      return mProducts
    },
  },

  shopPopulate: {
    type: _mShopTC,
    args: { id: "ID" },
    resolve: async (source, args, context, info) => {
      const shop = await Shop.findById(args.id)

      const products = await Product.find({ "shopsMeta.shop": args.id })

      const getProducts = () => {
        let _products = []

        products.forEach((product) => {
          let _product = {
            name: product.name,
            price: product.shopsMeta.filter(
              (shopMeta) => shopMeta.shop == args.id
            )[0].price,
            id: product.id,
          }

          _products.push(_product)
        })

        return _products
      }

      return {
        id: shop.id,
        name: shop.name,
        email: shop.email,
        password: shop.password,
        phoneNumber: shop.phoneNumber,
        products: getProducts(),
      }
    },
  },
})

schemaComposer.Mutation.addFields({
  shopAddProduct: {
    type: ProductTC,
    args: {
      shopId: "ID",
      price: "Float",
      productId: "ID",
      name: "String",
      category: "String",
    },
    resolve: async (source, args, context, info) => {
      if (args.productId !== "") {
        const updatedProduct = await Product.updateOne(
          { _id: args.productId },
          {
            $push: {
              shopsMeta: {
                shop: args.shopId,
                price: args.price,
              },
            },
          }
        )
        if (!updatedProduct) return null // or gracefully return an error etc...
        return Product.findOne({ _id: args.productId }) // return the record
      } else {
        const newProduct = new Product({
          name: args.name,
          category: args.category,
          shopsMeta: [
            {
              shop: args.shopId,
              price: args.price,
            },
          ],
        })

        let doc = newProduct.save().then((doc) => {
          return doc
        })

        return doc
      }
    },
  },

  productUpdatePrice: {
    type: "String",
    args: { shopId: "ID", price: "Float", productId: "ID" },
    resolve: async (source, args, context, info) => {
      const updatedProduct = await Product.updateOne(
        { "shopsMeta.shop": args.shopId },
        {
          $set: {
            "shopsMeta.$.price": args.price,
          },
        }
      )

      if (!updatedProduct) return null
      return "Success"
    },
  },

  productDelete: {
    type: "String",
    args: { shopId: "ID", productId: "ID" },
    resolve: async (source, args, context, info) => {
      const updatedProduct = await Product.updateOne(
        { _id: args.productId },
        {
          $pull: { shopsMeta: { shop: args.shopId } },
        },
        { safe: true, multi: true }
      )

      if (!updatedProduct) return null
      return "Success"
    },
  },
})

schemaComposer.Subscription.addFields({
  cartUpdated: {
    type: CartTC,
    args: null,
    resolver: async (source, args, context, info) => {
      return "Hey"
    },
    subscribe: () => pubsub.asyncIterator(["CART_UPDATED"]),
  },
  todoCreated: {
    type: [TodoTC],
    subscribe: () => pubsub.asyncIterator(["TODO_ADDED"]),
  },
})

export default schemaComposer.buildSchema()

export { pubsub }
