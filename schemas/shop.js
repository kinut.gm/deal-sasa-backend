import { Shop, ShopTC } from "../models/shop.js"

const ShopQuery = {
  shopById: ShopTC.getResolver("findById"),
  shopByIds: ShopTC.getResolver("findByIds"),
  shopOne: ShopTC.getResolver("findOne"),
  shopMany: ShopTC.getResolver("findMany"),
  shopCount: ShopTC.getResolver("count"),
  shopConnection: ShopTC.getResolver("connection"),
  shopPagination: ShopTC.getResolver("pagination"),
}

const ShopMutation = {
  shopCreateOne: ShopTC.getResolver("createOne"),
  shopCreateMany: ShopTC.getResolver("createMany"),
  shopUpdateById: ShopTC.getResolver("updateById"),
  shopUpdateOne: ShopTC.getResolver("updateOne"),
  shopUpdateMany: ShopTC.getResolver("updateMany"),
  shopRemoveById: ShopTC.getResolver("removeById"),
  shopRemoveOne: ShopTC.getResolver("removeOne"),
  shopRemoveMany: ShopTC.getResolver("removeMany"),
}

export { ShopQuery, ShopMutation }
