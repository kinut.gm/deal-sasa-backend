import { Cart, CartTC } from "../models/cart.js"
import { CustomerTC } from "../models/customer.js"
import { Product, ProductTC } from "../models/product.js"
import { ShopTC } from "../models/shop.js"

import { pubsub } from "./main.js"

const CartQuery = {
  cartById: CartTC.getResolver("findById"),
  cartByIds: CartTC.getResolver("findByIds"),
  cartOne: CartTC.getResolver("findOne"),
  cartMany: CartTC.getResolver("findMany"),
  cartCount: CartTC.getResolver("count"),
  cartConnection: CartTC.getResolver("connection"),
  cartPagination: CartTC.getResolver("pagination"),
}

const CartMutation = {
  cartCreateOne: CartTC.getResolver("createOne"),
  cartCreateMany: CartTC.getResolver("createMany"),
  cartUpdateById: CartTC.getResolver("updateById"),
  cartUpdateOne: () => {
    pubsub.publish("CART_UPDATED", {
      cartUpdated: CartTC.getResolver("updateOne"),
    })
    return CartTC.getResolver("updateOne")
  },
  cartUpdateMany: CartTC.getResolver("updateMany"),
  cartRemoveById: CartTC.getResolver("removeById"),
  cartRemoveOne: CartTC.getResolver("removeOne"),
  cartRemoveMany: CartTC.getResolver("removeMany"),
}

CartTC.addRelation("product", {
  resolver: () => ProductTC.getResolver("findOne"),
  prepareArgs: {
    id: (source) => source.productId.valueOf(),
  },
  projection: { productId: true },
})

CartTC.addRelation("shop", {
  resolver: () => ShopTC.getResolver("findOne"),
  prepareArgs: {
    id: (source) => source.shopId.valueOf(),
  },
  projection: { shopId: true },
})

CartTC.addRelation("customer", {
  resolver: () => CustomerTC.getResolver("findOne"),
  prepareArgs: {
    id: (source) => source.customerId.valueOf(),
  },
  projection: { customerId: true },
})

export { CartQuery, CartMutation }
