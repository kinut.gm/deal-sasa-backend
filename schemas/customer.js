import { CartTC, Cart } from "../models/cart.js"
import { Customer, CustomerTC } from "../models/customer.js"

const CustomerQuery = {
  customerById: CustomerTC.getResolver("findById"),
  customerByIds: CustomerTC.getResolver("findByIds"),
  customerOne: CustomerTC.getResolver("findOne"),
  customerMany: CustomerTC.getResolver("findMany"),
  customerCount: CustomerTC.getResolver("count"),
  customerConnection: CustomerTC.getResolver("connection"),
  customerPagination: CustomerTC.getResolver("pagination"),
}

const CustomerMutation = {
  customerCreateOne: CustomerTC.getResolver("createOne"),
  customerCreateMany: CustomerTC.getResolver("createMany"),
  customerUpdateById: CustomerTC.getResolver("updateById"),
  customerUpdateOne: CustomerTC.getResolver("updateOne"),
  customerUpdateMany: CustomerTC.getResolver("updateMany"),
  customerRemoveById: CustomerTC.getResolver("removeById"),
  customerRemoveOne: CustomerTC.getResolver("removeOne"),
  customerRemoveMany: CustomerTC.getResolver("removeMany"),
}

CustomerTC.addFields({
  cart: {
    type: [CartTC], // array of Cart Items
    resolve: async (customer, args, context, info) => {
      const docs = await Cart.find({ customerId: customer.id.valueOf() })
      return docs
    },
  },
})

export { CustomerQuery, CustomerMutation }
