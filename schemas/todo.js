import { Todo, TodoTC } from "../models/todo.js"

import { pubsub } from "./main.js"

const TodoQuery = {
  todoOne: TodoTC.getResolver("findOne"),
  todoMany: TodoTC.getResolver("findMany"),
  todoCount: TodoTC.getResolver("count"),
}

const TodoMutation = {
  todoCreateOne: () => {
    pubsub.publish("TODO_ADDED", {
      todoCreated: TodoTC.getResolver("createOne"),
    })
    return TodoTC.getResolver("createOne")
  },
  todoUpdateOne: TodoTC.getResolver("updateOne"),
  todoRemoveOne: TodoTC.getResolver("removeOne"),
}

export { TodoQuery, TodoMutation }
