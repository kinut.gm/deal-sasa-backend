import { Product, ProductTC } from "../models/product.js"
import { Shop, ShopTC } from "../models/shop.js"

const ProductQuery = {
  productById: ProductTC.getResolver("findById"),
  productByIds: ProductTC.getResolver("findByIds"),
  productOne: ProductTC.getResolver("findOne"),
  productMany: ProductTC.getResolver("findMany"),
  productCount: ProductTC.getResolver("count"),
  productConnection: ProductTC.getResolver("connection"),
  productPagination: ProductTC.getResolver("pagination"),
}

const ProductMutation = {
  productCreateOne: ProductTC.getResolver("createOne"),
  productCreateMany: ProductTC.getResolver("createMany"),
  productUpdateById: ProductTC.getResolver("updateById"),
  productUpdateOne: ProductTC.getResolver("updateOne"),
  productUpdateMany: ProductTC.getResolver("updateMany"),
  productRemoveById: ProductTC.getResolver("removeById"),
  productRemoveOne: ProductTC.getResolver("removeOne"),
  productRemoveMany: ProductTC.getResolver("removeMany"),
}

ProductTC.getFieldOTC("shopsMeta").addRelation("shop", {
  resolver: () => ShopTC.getResolver("findOne"),
  prepareArgs: {
    id: (source) => {
      return source.shop.valueOf()
    },
  },
  projection: { shop: true },
})

ProductTC.getFieldOTC("shopsMeta").addFields({
  details: {
    type: ShopTC || null,
    resolve: async (shopMeta, args, context, info) => {
      const doc = await Shop.findOne({
        id: shopMeta.shop.valueOf(),
      })

      return doc
    },
  },
})

export { ProductQuery, ProductMutation }
