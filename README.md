This is a Express + MongoDB + GraphQL backend project for Deal Sasa.

## Getting Started

First , make sure you have Node.js runtime and MongoDB installed.

Second , install dependancies by running inside the project's root folder :

```bash
npm install --save

```

Third, run the development server:

```bash
npm start

```

Open [http://localhost:8000](http://localhost:3000) with your browser to see Deal Sasa's GraphQL playground.
